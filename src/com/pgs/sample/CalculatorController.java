package com.pgs.sample;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/calc")
public class CalculatorController {

	@RequestMapping(value = "/add/{arg1}/{arg2}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OperationResult> addTwoArgs(@PathVariable("arg1") Integer arg1, @PathVariable("arg2") Integer arg2) {
        return new ResponseEntity<OperationResult>(new OperationResult(arg1 + arg2, OperationType.ADD), HttpStatus.OK);
    }

	@RequestMapping(value = "/sub/{arg1}/{arg2}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OperationResult> subTwoArgs(@PathVariable("arg1") Integer arg1, @PathVariable("arg2") Integer arg2) {
        return new ResponseEntity<OperationResult>(new OperationResult(arg1 - arg2, OperationType.SUB), HttpStatus.OK);
    }

	@RequestMapping(value = "/div/{arg1}/{arg2}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OperationResult> divTwoArgs(@PathVariable("arg1") Integer arg1, @PathVariable("arg2") Integer arg2) {
		if(arg2 == 0)
			return new ResponseEntity<OperationResult>(new OperationResult(0, OperationType.DIV), HttpStatus.BAD_REQUEST);
        
        return new ResponseEntity<OperationResult>(new OperationResult(arg1 / arg2, OperationType.DIV), HttpStatus.OK);
    }

	@RequestMapping(value = "/mult/{arg1}/{arg2}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OperationResult> multTwoArgs(@PathVariable("arg1") Integer arg1, @PathVariable("arg2") Integer arg2) {
        return new ResponseEntity<OperationResult>(new OperationResult(arg1 * arg2, OperationType.MULT), HttpStatus.OK);
    }

	@RequestMapping(value = "/pow/{arg1}/{arg2}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OperationResult> powTwoArgs(@PathVariable("arg1") Integer arg1, @PathVariable("arg2") Integer arg2) {
        return new ResponseEntity<OperationResult>(new OperationResult((int)Math.pow(arg1, arg2), OperationType.POW), HttpStatus.OK);
    }
}
