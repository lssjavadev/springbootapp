package com.pgs.sample;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherWrapper {
	@JsonProperty("coord")
	private WeatherWrapperCoord coord;
	@JsonProperty("weather")
	private WeatherWrapperWeather weather[];
	@JsonProperty("main")
	private WeatherWrapperMain main;
	@JsonProperty("sys")
	private WeatherWrapperSys sys;
	@JsonProperty("name")
	private String name;
	
	
	public WeatherWrapperCoord getCoord() {
		return coord;
	}
	public void setCoord(WeatherWrapperCoord coord) {
		this.coord = coord;
	}
	public WeatherWrapperWeather[] getWeather() {
		return weather;
	}
	public void setWeather(WeatherWrapperWeather[] weather) {
		this.weather = weather;
	}
	public WeatherWrapperMain getMain() {
		return main;
	}
	public void setMain(WeatherWrapperMain main) {
		this.main = main;
	}
	public WeatherWrapperSys getSys() {
		return sys;
	}
	public void setSys(WeatherWrapperSys sys) {
		this.sys = sys;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
