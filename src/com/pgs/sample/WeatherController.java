package com.pgs.sample;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/weather")
public class WeatherController {

	private static final String appId = "&appid=e3d0cd84a671df6706225b7a6c08c72f";
	
	@RequestMapping(value = "/city/{arg}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<WeatherWrapper> byCity(@PathVariable("arg") String arg) {
		
		RestTemplate restTemplate = new RestTemplate();

		WeatherWrapper wrapper = restTemplate.getForObject(
				"http://api.openweathermap.org/data/2.5/weather?q=" + arg + appId,
				WeatherWrapper.class);

		return new ResponseEntity<WeatherWrapper>(wrapper, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/coord/{arg_lat}/{arg_lon}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<WeatherWrapper> byCoord(@PathVariable("arg_lat") String arg_lat, @PathVariable("arg_lon") String arg_lon) {
		
		RestTemplate restTemplate = new RestTemplate();

		WeatherWrapper wrapper = restTemplate.getForObject(
				"http://api.openweathermap.org/data/2.5/weather?lat=" + arg_lat + "&lon=" + arg_lon + appId,
				WeatherWrapper.class);

		return new ResponseEntity<WeatherWrapper>(wrapper, HttpStatus.OK);
    }
}
