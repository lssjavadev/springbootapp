package com.pgs.sample;

public enum OperationType {
	ADD,
	SUB,
	DIV,
	MULT,
	POW
}
